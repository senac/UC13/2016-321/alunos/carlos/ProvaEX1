
package br.com.casa.ex1.test;

import br.com.casa.ex1.Calculadora;

import br.com.casa.ex1.Pessoa;
import org.junit.Test;
import static org.junit.Assert.*;


public class CalculadoraTest {
    
    @Test
    public void deveMostarAPessoaMaisVelha(){
        Calculadora calculadora = new Calculadora();
       
            
        String resultado = "Luciene";
        assertEquals(calculadora.faixaMaisVelha(), resultado);
        
    }
    

 @Test
    public void deveMostarAPessoaMaisNova(){
        Calculadora calculadora = new Calculadora();
        Pessoa pessoa = new Pessoa();
        
        String resultado = "Marcia";
        assertEquals(calculadora.faixaMaisNova(), resultado);
        
    }
    
}
